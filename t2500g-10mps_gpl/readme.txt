TP-LINK T2500G-10MPS(UN) 1.0 GPL code readme

1. This package contains GPL code for T2500G-10MPS(UN) 1.0
2. All components have been built successfully on CentOS Linux release 6.0

Build Instructions
1. All build targets are in "t2500g-10mps_gpl/tplink/buildroot-realtek/", you should enter the directory to build components.

2. Toolchain binaries are avaliable in this package. The directory is "t2500g-10mps_gpl/tplink/buildroot-realtek/ext-tools/msdk-4.3.6-mips-EB-2.6.32-0.9.33/host/usr/bin/".

3. Building steps:
 1) put t2500g-10mps_gpl in directory /project/trunk
 2) cd /project/trunk/t2500g-10mps_gpl/tplink/buildroot-realtek
 3) make O=build/t2500g-10mps tplink-t2500g-10mps_defconfig
 4) make O=build/t2500g-10mps
 After step4 completed, The uboot, linux kernel image, rootfs filesystem could be found in directory "t2500g-10mps_gpl/tplink/buildroot-realtek/build/t2500g-10mps/images".




