################################################################################
#
# sdk
#
################################################################################

# you can override vars/cmds in tplink/sdk/sdk.mk

################################################################################
# vars/cmds
################################################################################
SDK_SITE		= $(call qstrip,$(BR2_TPLINK_SDK_DIR))
SDK_SITE_METHOD = local

SDKCFGDIR		= $(SDK_DIR)/config
SDKINCCOM		= $(SDK_DIR)/include/common
SDK_CONFIG		= $(SDKCFGDIR)/.config
SDK_BUILD		= $(SDK_DIR)/build
SDK_LIBDIR		= $(SDK_DIR)/lib
SDK_USRDIR		= $(SDK_DIR)/user/switch/sdk
SDK_USR_SW_DIR = $(SDK_DIR)/user/switch
DRV_INCLUDE		= $(SDK_DIR)/system/linux

SDK_LIBRARIES += $(STATIC_LIB_PATH)/librtcore.a $(STATIC_LIB_PATH)/librtk.a

define SDK_CONFIGURE_CMDS
	@echo "configure sdk prev ###############################"
	$(MAKE) -C $(SDKCFGDIR) menuconfig SILENT_CFG=1
	@echo "configure sdk post ###############################"
endef

define SDK_BUILD_CMDS
	:
endef

define SDK_INSTALL_TARGET_CMDS
	:
endef

# can be overridden
define SDK_CONFIG_PATCH_CMDS
	:
endef

################################################################################
# rules
################################################################################
.PHONY: sdk.defconfig

sdk.defconfig: dirs sdk-depends
	@echo "sdk.defconfig prev ###############################"
	@if [ ! -e $(SDK_DIR)/config/.config ]; then \
		echo "cp sdk default config"; \
		cp -f $(BR2_BOARD_DEF_CFG_STRIP)/sdk.config $(SDK_DIR)/config/.config; \
		$(SDK_CONFIG_PATCH_CMDS); \
	fi
	@echo "sdk.defconfig post ###############################"

sdk-configure: sdk.defconfig


