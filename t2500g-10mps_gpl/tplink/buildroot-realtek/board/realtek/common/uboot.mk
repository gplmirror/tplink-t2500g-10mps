################################################################################
#
# U-Boot
#
################################################################################

# you can override vars/cmds in boot/uboot/uboot.mk

################################################################################
# vars/cmds
################################################################################
UBOOT_CONFIG_TARGET = menuconfig SILENT_CFG=1

################################################################################
# rules
################################################################################
.PHONY: uboot.defconfig uboot-menuconfig

# can be overridden
define UBOOT_CONFIG_PATCH_CMDS
	:
endef

# copy uboot default config if not exist
uboot.defconfig: dirs uboot-depends
	@echo "uboot.defconfig prev #############################"
	@if [ ! -e $(UBOOT_DIR)/.config ]; then \
		cp -f $(BR2_BOARD_DEF_CFG_STRIP)/u-boot.config $(UBOOT_DIR)/.config; \
		$(call KCONFIG_SET_OPT,CONFIG_BOARDMODEL,$(UBOOT_BOARDMODEL),$(UBOOT_DIR)/.config);\
		$(UBOOT_CONFIG_PATCH_CMDS);\
	fi
	@echo "uboot.defconfig post #############################"

# sdk-depends -> ln -snf $(SDKSYSDIR)/include/linux/mtd/rtk_flash_common.h $(SRCTREE)/board/Realtek/include/rtk_flash_common.h
uboot-configure: uboot.defconfig sdk-depends

uboot-menuconfig: uboot.defconfig sdk-depends
	$(TARGET_CONFIGURE_OPTS) $(UBOOT_CONFIGURE_OPTS) 	\
		$(MAKE) -C $(UBOOT_DIR) $(UBOOT_MAKE_OPTS) menuconfig
	rm -f $(UBOOT_DIR)/.stamp_{built,target_installed,images_installed}

