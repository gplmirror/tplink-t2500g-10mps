/*
 * Copyright(c) Realtek Semiconductor Corporation, 2008
 * All rights reserved.
 *
 * Purpose : Related implementation of the RTL8214F PHY driver.
 *
 * Feature : RTL8214F PHY driver
 *
 */


/*
 * Include Files
 */
#include <rtk_osal.h>
#include <rtk_debug.h>
#include <rtk_switch.h>
#include <common/util.h>
#include <rtk/phy/conf/conftypes.h>
#include <config.h>


/*
 * Symbol Definition
 */

/*
 * Data Declaration
 */
extern const rtk_switch_model_t *gSwitchModel;
extern const rtk_mac_drv_t *gMacDrv;

/*
 * Macro Definition
 */

/*
 * Function Declaration
 */

/* Function Name:
 *      rtl8218c_config
 * Description:
 *      Configuration code for RTL8218c.
 * Input:
 *      macId - the macId of PHY0 of the RTL8218b
 * Output:
 *      None
 * Return:
 *      None
 * Note:
 *      None
 */
void rtl8218c_config(Tuint8 macId)
{
    int rtl8218_phy0_macid = macId - (macId % 8);
    int port;

    OSAL_PRINTF("### RTL8218C config - MAC ID = %d ###\n", rtl8218_phy0_macid);

    gMacDrv->drv_miim_write(rtl8218_phy0_macid, gMacDrv->miim_max_page, 30, 0);
    for (port = 0; port < 8; ++port)
    {
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xbc0, 19, 0x180);
    }

    gMacDrv->drv_miim_write(rtl8218_phy0_macid + 1, 0xbc6, 22, 0x3e00);
    gMacDrv->drv_miim_write(rtl8218_phy0_macid + 5, 0xbc7, 22, 0x3e00);

    for (port = 0; port < 8; ++port)
    {
        if ((port % 2) == 0)
        {
            gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa42, 22, 0xf80);
        }
        else
        {
            gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa42, 22, 0xf40);
        }
    }

    for (port = 0; port < 8; ++port)
    {
        rtk_phyPatchBit_set(rtl8218_phy0_macid + port, 0xa84, 18, 6, 4, 2);
    }

    for (port = 0; port < 8; ++port)
    {
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xbcd, 22, 0x6666);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xbcd, 23, 0x6666);
    }

    for (port = 0; port < 8; ++port)
    {
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xbcc, 16, 0x2600);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xbca, 23, 0x3000);
    }

    for (port = 0; port < 8; ++port)
    {
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 27, 0x8062);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 28, 0x2000);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 27, 0x806a);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 28, 0x2300);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 27, 0x8072);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 28, 0x2300);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 27, 0x8099);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 28, 0x2e00);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 27, 0x809b);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 28, 0x84e5);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 27, 0x809d);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 28, 0x08a1);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 27, 0x80a0);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 28, 0x018f);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 27, 0x80ab);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 28, 0x2e00);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 27, 0x80ad);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 28, 0x7c06);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 27, 0x80af);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 28, 0x08a1);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 27, 0x80b2);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 28, 0x017f);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 27, 0x80b0);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 28, 0xdc06);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 27, 0x808d);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 28, 0x081a);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 27, 0x808a);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 28, 0x470a);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 27, 0x8088);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xa43, 28, 0xa09b);
    }

    for (port = 0; port < 8; ++port)
    {
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xbc1, 18,  0xa000);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, 0xbc1, 19,  0x5077);
    }

    gMacDrv->drv_miim_write(rtl8218_phy0_macid, gMacDrv->miim_max_page, 30, 8);
    gMacDrv->drv_miim_write(rtl8218_phy0_macid, 0x3B2, 18, 0x1f1f);
    gMacDrv->drv_miim_write(rtl8218_phy0_macid, 0x4A8, 17, 0x6C22);
    gMacDrv->drv_miim_write(rtl8218_phy0_macid, 0x5A8, 22, 0x6C22);
    gMacDrv->drv_miim_write(rtl8218_phy0_macid, 0x584, 17, 0xCC90);
    gMacDrv->drv_miim_write(rtl8218_phy0_macid, 0x584, 20, 0x8DC3);
    gMacDrv->drv_miim_write(rtl8218_phy0_macid, 0x585, 17, 0xC226);
    gMacDrv->drv_miim_write(rtl8218_phy0_macid, 0x5A8, 16, 0x8003);
    gMacDrv->drv_miim_write(rtl8218_phy0_macid, 0x5A8, 17, 0x848C);
    gMacDrv->drv_miim_write(rtl8218_phy0_macid, 0x5A8, 19, 0x9DBE);
    gMacDrv->drv_miim_write(rtl8218_phy0_macid, 0x5A8, 21, 0x0025);

    rtk_phyPatchBit_set(rtl8218_phy0_macid, 0x4a8, 0x12, 10, 8, 0x0);
    rtk_phyPatchBit_set(rtl8218_phy0_macid, 0x4a8, 0x12,  7, 5, 0x3);
    rtk_phyPatchBit_set(rtl8218_phy0_macid, 0x4a8, 0x12,  4, 2, 0x0);
    rtk_phyPatchBit_set(rtl8218_phy0_macid, 0x5a8, 0x17, 10, 8, 0x0);
    rtk_phyPatchBit_set(rtl8218_phy0_macid, 0x5a8, 0x17,  7, 5, 0x3);
    rtk_phyPatchBit_set(rtl8218_phy0_macid, 0x5a8, 0x17,  4, 2, 0x0);

    gMacDrv->drv_miim_write(rtl8218_phy0_macid, 0x3B2, 18, 0x0606);
    gMacDrv->drv_miim_write(rtl8218_phy0_macid, gMacDrv->miim_max_page, 30, 0);

    for (port = 0; port < 8; ++port)
    {
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, gMacDrv->miim_max_page, 30, 1);
        rtk_phyPatchBit_set(rtl8218_phy0_macid + port, 0xa42, 0x14, 9, 9, 0x0);
        gMacDrv->drv_miim_write(rtl8218_phy0_macid + port, gMacDrv->miim_max_page, 30, 0);
    }

    return;
} /* end of rtl8218c_config */
